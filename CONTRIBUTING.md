Hello, and welcome to my repo! Feel free to contribute by opening a PR. Check out the list below for some guidelines!

### PR Guidelines

# Overview

## Relate your change to a JIRA card
https://crosschx.atlassian.net/secure/RapidBoard.jspa

## Relate a single PR to a single, atomic task
Try not to make changes to files unrelated to your task

## Provide a helpful description
Please provide the following, where applicable:

* Summary of changes
* Screenshots of UI changes
* Implications of this change

## Update release notes
Don't forget to include dependency changes when relevant!

# Security

## Ensure users are _authenticated and authorized_

## Audit user actions when possible

## Sensitive data management

When passwords, PHI, or other sensitive data is being handled, ensure that they are encrypted/hashed/salted, etc. both at rest and in transit.

## Communication security

## Data protection

## Input validation

## General secure coding practices

# Product / Feature

## When making an overarching architecture change, get buy-in from your team / the Chiefs

## Only make feature changes that will benefit users, or tech-debt changes that will help developers!

# Adherence to internal conventions

# Code maintainability / syntax

# Testing

## Make sure your code compiles/runs/does what it should do!

## Test all potential paths

## Test in all relevant environments
Mac, Windows, etc. If this was in response to a bug found on a customer machine or in a specific environment, test it there.