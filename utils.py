from matplotlib import pyplot as plt
from mxnet import nd
import numpy as np


def transform(data, label):
    return data.astype(np.float32)/255, label.astype(np.float32)


def setup_plot():
    plt.ion()
    fig = plt.figure(figsize=(10, 8))
    ax1 = fig.add_subplot(2, 1, 1)
    plt.subplots_adjust(bottom=0)
    plt.show()
    return plt, ax1


def draw_plot(plt, ax1, losses, t_accuracies, v_accuracies, filename):
    ax1.cla()
    plt.cla()
    ax1.set_xlabel("Iterations")
    ax1.set_ylim([0,100])
    ax1.set_title("MNIST Dataset Classification")
    ax1.plot(list(range(0, len(losses))), losses, label='loss')
    ax1.plot(list(range(0, len(t_accuracies))), t_accuracies, label='training accuracy')
    ax1.plot(list(range(0, len(v_accuracies))), v_accuracies, label='validation accuracy')
    ax1.legend(loc=1)
    # axbox = plt.axes([0.1, 0.05, 0.8, 0.4])
    # text = ''
    # for e in epoch_stats:
    #     if len(text) > 0:
    #         text = '%s\n%s' % (text, e)
    #     else:
    #         text = '%s' % (e)
    # text_box = TextBox(axbox, '', initial=text)
    plt.tight_layout()

    plt.draw()
    plt.pause(0.001)
    plt.savefig(f"./plot/{filename}")

    return plt


def display_data(data):
    im = nd.transpose(data,(1,0,2,3))
    im = nd.reshape(im,(28,10*28,1))
    imtiles = nd.tile(im, (1,1,3))

    plt.ion()
    plt.imshow(imtiles.asnumpy())
    plt.show()
    plt.pause(.001)
    input("Press [enter] to view results.")