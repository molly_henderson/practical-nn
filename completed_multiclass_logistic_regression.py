from __future__ import print_function
import mxnet as mx
from mxnet import autograd, nd, gluon
from mxnet.gluon.data import DataLoader
from mxnet.metric import Accuracy
import numpy as np
from utils import draw_plot, setup_plot, transform


###        HYPERPARAMETERS        ###
### feel free to fiddle with them ###
epochs = 10             # Number of times to run through our entire training set
batch_size = 64         # Number of training examples to run through at a time
sigma = 1.              # Standard deviation of the normal distribution used for parameter initialization
learning_rate = 0.1     # Learning rate to use in the Stochastic Gradient Descent trainer function


###            SETTINGS           ###
show_plot = True        # Do we display a plot of loss and accuracy? (slows down the process)
validate = False        # Do we plot extra "validation" information? (slows down the process)
record_frequency = 100  # How many iterations do we wait to plot new information?


###     these are just consts     ###
###   probably don't change them  ###
num_inputs = 784        # Size of the input to the NN. Corresponds to the number of pixels in one image
num_outputs = 10        # Number of classes in our problem (i.e., # of labels)
num_examples = 60000    # Total number of labeled examples we have
model_file = 'model.params'
plot_file = 'multiclass_logistic_regression.png'


def build_net():
    net = gluon.nn.Sequential()
    with net.name_scope():
        net.add(gluon.nn.Dense(num_outputs))
    return net


def load_data():
    train_data = DataLoader(mx.gluon.data.vision.MNIST(train=True, transform=transform), batch_size, shuffle=True)
    validation_data = DataLoader(mx.gluon.data.vision.MNIST(train=False, transform=transform), batch_size, shuffle=False)
    return train_data, validation_data


def initialize_params(net, ctx):
    ### Use the following line instead if you have an existing model to build off of ###
    # net.load_params(model_file, ctx)
    initializer = mx.init.Normal(sigma=sigma)
    net.collect_params().initialize(initializer, ctx=ctx)


def evaluate_accuracy(output, labels):
    acc = Accuracy()
    predictions = nd.argmax(output, axis=1)
    acc.update(preds=predictions, labels=labels)
    return acc.get()[1]

def evaluate_validation_accuracy(data, net, ctx):
    # really just grab one batch of validation data
    for i, (data, labels) in enumerate(data):
        data = data.as_in_context(ctx).reshape((-1, num_inputs))
        labels = labels.as_in_context(ctx)
        output = net(data)
        return evaluate_accuracy(output, labels)


def train_net():
    ### Using a machine with a GPU? Update this to mx.gpu()! ###
    ctx = mx.cpu()

    net = build_net()
    initialize_params(net, ctx)

    train_data, validation_data = load_data()
    loss_function = gluon.loss.SoftmaxCrossEntropyLoss()
    trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': learning_rate})

    if show_plot:
        plt, ax1 = setup_plot()

    t_accuracies = []
    v_accuracies = []
    losses = []

    ### Here we will: ###
    ###     Loop through epochs
    ###     Loop through training data
    ###     Load data and labels into the context
    ###     Run data through net
    ###     Calculate loss
    ###     Backpropagate; make a step of parameter updates
    ###     Calculate accuracy

    for e in range(epochs):
        epoch_losses = []
        epoch_t_accs = []
        epoch_v_accs = []
        for i, (data, labels) in enumerate(train_data):
            data = data.as_in_context(ctx).reshape((-1, num_inputs))
            labels = labels.as_in_context(ctx)
            with autograd.record():
                output = net(data)
                loss = loss_function(output, labels)
            loss.backward()
            trainer.step(data.shape[0])

            epoch_losses.append(nd.mean(loss).asscalar())
            epoch_t_accs.append(evaluate_accuracy(output, labels))
            if validate:
                epoch_v_accs.append(evaluate_validation_accuracy(validation_data, net, ctx))

            if i % record_frequency == 0:
                losses.append(np.mean(epoch_losses))
                t_accuracies.append(np.mean(epoch_t_accs)*100)
                v_accuracies.append(np.mean(epoch_v_accs)*100)

                print(f"Epoch {e}. Iteration {i}.\t\tLoss: {losses[-1]},\tTrain_acc: {t_accuracies[-1]}")

                net.save_params(model_file)

                if show_plot:
                    plt = draw_plot(plt, ax1, losses, t_accuracies, v_accuracies, plot_file)


if __name__ == '__main__':
    train_net()
