from __future__ import print_function
import mxnet as mx
from mxnet import autograd, nd, gluon
from mxnet.gluon.data import DataLoader
from mxnet.metric import Accuracy
import numpy as np
from utils import draw_plot, setup_plot, transform


###        HYPERPARAMETERS        ###
### feel free to fiddle with them ###
epochs = 10             # Number of times to run through our entire training set
batch_size = 64         # Number of training examples to run through at a time
sigma = 1.              # Standard deviation of the normal distribution used for parameter initialization
learning_rate = 0.1     # Learning rate to use in the Stochastic Gradient Descent trainer function


###            SETTINGS           ###
show_plot = True        # Do we display a plot of loss and accuracy? (slows down the process)
validate = False        # Do we plot extra "validation" information? (slows down the process)
record_frequency = 100  # How many iterations do we wait to plot new information?


###     these are just consts     ###
###   probably don't change them  ###
num_inputs = 784        # Size of the input to the NN. Corresponds to the number of pixels in one image
num_outputs = 10        # Number of classes in our problem (i.e., # of labels)
num_examples = 60000    # Total number of labeled examples we have
model_file = 'model.params'
plot_file = 'multiclass_logistic_regression.png'


def build_net():
    # TODO: build and return a neural network yo
    return None


def load_data():
    # TODO: load and return train_data, validation_data
    return None, None


def initialize_params(net, ctx):
    # TODO: initialize internal NN params
    pass


def evaluate_accuracy(output, labels):
    # TODO: calculate and return the accuracy of our outputs
    return None


def evaluate_validation_accuracy(data, net, ctx):
    # really just grab one batch of validation data
    for i, (data, labels) in enumerate(data):
        data = data.as_in_context(ctx).reshape((-1, 784))
        labels = labels.as_in_context(ctx)
        output = net(data)
        return evaluate_accuracy(output, labels)


def train_net():
    ### Using a machine with a GPU? Update this to mx.gpu()! ###
    ctx = mx.cpu()

    net = build_net()
    initialize_params(net, ctx)

    train_data, validation_data = load_data()
    loss_function = gluon.loss.SoftmaxCrossEntropyLoss()
    trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': learning_rate})

    if show_plot:
        plt, ax1 = setup_plot()

    t_accuracies = []
    v_accuracies = []
    losses = []

    # TODO: these things
    ### Here we will: ###
    ###     Loop through epochs
    ###     Loop through training data
    ###     Load data and labels into the context
    ###     Run data through net
    ###     Calculate loss
    ###     Backpropagate; make a step of parameter updates
    ###     Calculate accuracy


if __name__ == '__main__':
    train_net()
