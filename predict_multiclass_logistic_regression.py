from multiclass_logistic_regression import build_net
import mxnet as mx
from mxnet import nd
from utils import display_data, transform


sample_size = 10
model_file = 'model.params'


def model_predict(net, data, ctx):
    output = net(data.as_in_context(ctx))
    return nd.argmax(output, axis=1)

ctx = mx.cpu()

net = build_net()
net.load_params(model_file, ctx=ctx)

# sample some random data points from the test set
sample_data = mx.gluon.data.DataLoader(mx.gluon.data.vision.MNIST(train=False, transform=transform), sample_size, shuffle=True)
for i, (data, labels) in enumerate(sample_data):
    data = data.as_in_context(ctx)
    display_data(data)

    pred=model_predict(net, data.reshape((-1, 784)), ctx)
    print('Actual values:', labels)
    print('Model predictions:', pred)
    break