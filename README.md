### Getting started

* Install Python 3:
```
$ brew install python3
```

* Install mxnet: (https://mxnet.apache.org/install/index.html)
```
$ pip3 install --upgrade pip
$ pip3 install --upgrade setuptools
$ pip3 install mxnet --pre --user
```

* Clone this repo! (Full disclosure, all the content probably won't be ready until 5 minutes before the workshop, so be prepared to git pull 😉)

This is a tutorial we'll be walking through! No need to look at it beforehand...

http://gluon.mxnet.io/chapter02_supervised-learning/softmax-regression-gluon.html
